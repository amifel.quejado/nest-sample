import { Controller, Get, Post, Body, Patch, Param, Delete, Req, Res, UseGuards, HttpException, HttpStatus, Query, ParseIntPipe, UsePipes } from '@nestjs/common';
import { ProductsService } from './products.service';
import { Request, Response } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { AuthRequest } from 'src/interfaces/request.interface';
import { ProductValidationPipe } from './products.validation.pipe';
import { ProductDTO } from './dtos/product.dto';
import { ProductQueryDTO } from './dtos/product-query.dto';

@UseGuards(JwtAuthGuard)
@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post('create')
  async create(@Req() req: AuthRequest, @Res() res: Response, @Body() body: ProductDTO) {
    try {
      const product = await this.productsService.create(body.name, body.type, req.user.sub);

      if (!product) throw new HttpException('Error creating product', HttpStatus.BAD_REQUEST);
      
      return res.status(HttpStatus.OK).json({
        message: 'Product successfully created',
        status: HttpStatus.OK,
        data: {
          product,
        }
      });
    } catch (error) {
      console.log('Error', JSON.stringify(error));
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error creating product',
        status: HttpStatus.BAD_REQUEST,
      });
    }
  }

  @Get()
  async findAll(@Req() req: AuthRequest, @Res() res: Response, @Query() query: ProductQueryDTO) {
    try {
      // console.log(query)
      const products = await this.productsService.findAll(query.offset, query.limit);

      return res.status(HttpStatus.OK).json({
        message: 'Products successfully retrieved',
        status: HttpStatus.OK,
        data: products
      });
    } catch (error) {
      console.log('Error', JSON.stringify(error, null, '\t'));
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error retrieving products',
        status: HttpStatus.BAD_REQUEST,
      });
    }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.productsService.findOne(+id);
  }
  
  // @UsePipes(ProductValidationPipe)
  @Patch(':id')
  async update(@Res() res: Response, @Param('id', ParseIntPipe) id: number, @Body() productBody: ProductDTO) {
    const product = await this.productsService.findOne(id)

    if (!product) throw new HttpException('Product not found', HttpStatus.NOT_FOUND);
    
    const updateQuery = await this.productsService.update(+id, {
      prodname: productBody.name,
      prodtype: productBody.type,
    });

    if (!updateQuery) throw new HttpException('Error updating product', HttpStatus.BAD_REQUEST);

    return res.status(HttpStatus.OK).json({
      message: 'Product successfully updated',
      status: HttpStatus.OK,
    }); 
  }

  @Delete(':id')
  async remove(@Req() req: AuthRequest, @Res() res: Response, @Param('id', new ParseIntPipe({exceptionFactory: (error) => new HttpException('Invalid product ID format', HttpStatus.BAD_REQUEST)})) id: number) {
    try {
      const deleted = await this.productsService.remove(+id);

      if (!deleted) throw new HttpException('No product deleted', HttpStatus.INTERNAL_SERVER_ERROR);

      return res.status(HttpStatus.OK).json({
        message: 'Products successfully deleted',
        status: HttpStatus.OK,
      });
    } catch (error) {
      console.log('Error', JSON.stringify(error, null, '\t'));
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error deleting products',
        status: HttpStatus.BAD_REQUEST,
      });
    }
  }
}
