import { Injectable } from '@nestjs/common';
import { User } from './user.model';
import { Sequelize } from 'sequelize-typescript';
import { InjectModel } from '@nestjs/sequelize';

@Injectable()
export class UserService {
  constructor(@InjectModel(User) private userModel: typeof User) {}

  findUser(username: string, password: string): Promise<User> {
    return this.userModel.findOne({
      where: {
        username,
        password,
      },
    });
  }

  findUserByToken(currenttoken: string): Promise<User> {
    return this.userModel.findOne({
      where: {
        currenttoken,
      },
    });
  }

  findUserByData(data: any): Promise<User> {
    return this.userModel.findOne({
      where: data,
    });
  }

  async create(username: string, password: string): Promise<User> {
    const user = await this.userModel.create({
      username,
      password,
    });
    // console.log(JSON.stringify(user, null, '\t'));
    return user;
  }

  async update(userid: number, data: any): Promise<boolean> {
    const query = await this.userModel.update(data, {
      where: {
        userid,
      }
    });
    // console.log(JSON.stringify(query[0], null, '\t'));
    if (query[0] > 0) return true;
    return false
  }
}
