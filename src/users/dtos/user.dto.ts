import { ApiProperty } from "@nestjs/swagger";
import { IsAlphanumeric, IsNotEmpty, IsString } from "class-validator";

export class UserDTO {
  @ApiProperty()
  @IsNotEmpty({ message: 'Username is required' })
  @IsAlphanumeric('en-US', { message: 'Username must include letters and numbers only' })
  username: string;

  @ApiProperty()
  @IsNotEmpty({ message: 'Password is required' })
  @IsString({ message: 'Password must include letters, numbers and characters only' })
  password: string;
}