import { ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ExtractJwt } from 'passport-jwt';
import { Observable } from 'rxjs';
import { UserService } from 'src/users/user.service';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private usersService: UserService) {
    super();
  }
  // canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
  //   const request = context.switchToHttp().getRequest();

  //   // console.log('JWT Auth guard', request);
  //   // console.log(request.headers.authorization.split(' ')[1]);
    
  //   // const sessionuser = this.usersService.findUserByToken(request.headers.authorization.split(' ')[1]).then((user) => console.log('Session user from then', user))
  //   // console.log('Session user', sessionuser)
  //   return super.canActivate(context);
  // }

  handleRequest(err, user, info) {
    // You can throw an exception based on either "info" or "err" arguments
    console.log(err, user, info)
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
