// import { ValidationPipe } from './validation.pipe';
import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as cookieParser from 'cookie-parser';
import { AppModule } from './app.module';
import { logger } from './logger.middleware';
import { Product } from './products/products.model';
import { User } from './users/user.model';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.use([logger, cookieParser()])
  app.useGlobalPipes(new ValidationPipe())

  const config = new DocumentBuilder()
    .setTitle('nest-sample API')
    .setDescription('Nest JS + TS with SOLID principles (hopefully)')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config, {
    extraModels: [User, Product]
  });
  SwaggerModule.setup('api', app, document);
  
  await app.listen(3001);
}
bootstrap();
