import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { User } from './users/user.model';
import { UsersModule } from './users/user.module';
import { ConfigModule } from '@nestjs/config';
import { AuthMiddleware } from './auth/auth.middleware';
import { UserController } from './users/user.controller';
import { ProductsModule } from './products/products.module';
import { Product } from './products/products.model';
import { MulterModule } from '@nestjs/platform-express';
import { APP_PIPE } from '@nestjs/core';
import { ValidationPipe } from './validation.pipe';
import { ProductsController } from './products/products.controller';

@Module({
  imports: [
    SequelizeModule.forFeature([User]),
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: '0.0.0.0',
      port: 3306,
      username: 'root',
      password: 'P@ssword123',
      database: 'testdb_nest',
      autoLoadModels: true,
      define: {
        timestamps: false,
      },
    }),
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    UsersModule,
    ProductsModule,
    MulterModule.register({
      dest: './uploads'
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    // { provide: APP_PIPE, useClass: ValidationPipe },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
      consumer.apply(AuthMiddleware)
      .exclude(
        { path: 'users/register', method: RequestMethod.POST },
        { path: 'users/login', method: RequestMethod.POST },
      )
      .forRoutes(UserController, ProductsController)
  }
}
